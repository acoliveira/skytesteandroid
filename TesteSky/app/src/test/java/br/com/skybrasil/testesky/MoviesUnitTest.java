package br.com.skybrasil.testesky;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import br.com.skybrasil.testesky.entities.Movie;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by alancostaoliveira on 12/6/17.
 */
public class MoviesUnitTest {

    @Mock
    MoviesPresenter moviesPresenter;
    @Mock
    MoviesContract.View view;
    @Mock
    Call<List<Movie>> call;

    @Captor
    private ArgumentCaptor<Callback<List<Movie>>> callbackArgumentCaptor;
    private List<Movie> movies;
    private Response<List<Movie>> response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        moviesPresenter = new MoviesPresenter(view, call);
    }

    @Test
    public void showMovies() throws Exception {
        Movie movie = new Movie();
        movie.setCoverUrl("https://image.tmdb.org/t/p/w1280/dsAQmTOCyMDgmiPp9J4aZTvvOJP.jpg");
        movie.setOverview("Stephen Strange (Benedict Cumberbatch) leva uma vida bem sucedida como neurocirurgião. Sua vida muda ");
        movie.setTitle("Doutor Estranho");

        movies = new ArrayList<>();
        movies.add(movie);
        response = Response.success(movies);

        moviesPresenter.loadMovies();
        verify(call).enqueue(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onResponse(call, response);

        verify(view).setProgressIndicator(false);
        verify(view).showMovies(movies);
    }

    @Test
    public void showError() {
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        ResponseBody body = ResponseBody.create(mediaType, "Unauthorized");

        response = Response.error(401, body);
        moviesPresenter.loadMovies();

        verify(call).enqueue(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onFailure(call, new Throwable("Unauthorized"));

        verify(view).setProgressIndicator(false);
        verify(view).showErro();
    }

    @Test
    public void showEmpytResult() {
        response = Response.success(movies);

        moviesPresenter.loadMovies();
        verify(call).enqueue(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onResponse(call, response);

        verify(view).setProgressIndicator(false);
        verify(view).showMovies(movies);
    }

}