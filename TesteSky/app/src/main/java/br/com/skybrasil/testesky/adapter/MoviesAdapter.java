package br.com.skybrasil.testesky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.skybrasil.testesky.R;
import br.com.skybrasil.testesky.entities.Movie;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    private Context context;
    private List<Movie> movies;

    public MoviesAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_movie, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewMovie;
        TextView labelTitle;
        Context context;

        private ViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;

            imageViewMovie = itemView.findViewById(R.id.imageViewMovie);
            labelTitle = itemView.findViewById(R.id.labelTitle);
        }

        private void bind(Movie movie) {
            Picasso.with(context).load(movie.getCoverUrl()).into(imageViewMovie);
            labelTitle.setText(movie.getTitle());
        }
    }
}
