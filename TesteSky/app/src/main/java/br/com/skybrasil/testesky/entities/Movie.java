package br.com.skybrasil.testesky.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public class Movie implements Serializable {
    private String title;
    private String overview;
    @SerializedName("cover_url")
    private String coverUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }
}
