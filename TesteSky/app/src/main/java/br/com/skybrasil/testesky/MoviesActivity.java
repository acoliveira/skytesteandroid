package br.com.skybrasil.testesky;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.com.skybrasil.testesky.adapter.MoviesAdapter;
import br.com.skybrasil.testesky.data.MovieServiceEndPoint;
import br.com.skybrasil.testesky.entities.Movie;
import retrofit2.Call;

public class MoviesActivity extends AppCompatActivity implements MoviesContract.View {

    private static MoviesContract.UserActionsListener listener;

    RecyclerView recyclerViewMovies;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        recyclerViewMovies = findViewById(R.id.recyclerViewMovies);
        progressBar = findViewById(R.id.progress);

        Call<List<Movie>> call = MovieServiceEndPoint.getMovies();
        listener = new MoviesPresenter(this, call);
        listener.loadMovies();
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMovies(List<Movie> movies) {
        MoviesAdapter moviesAdapter = new MoviesAdapter(MoviesActivity.this, movies);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(MoviesActivity.this, 2);
        recyclerViewMovies.setLayoutManager(gridLayoutManager);
        recyclerViewMovies.setHasFixedSize(true);
        recyclerViewMovies.setAdapter(moviesAdapter);
    }

    @Override
    public void showErro() {
        Toast.makeText(this, "Ocorreu um erro inesperado!", Toast.LENGTH_SHORT).show();
    }
}
