package br.com.skybrasil.testesky.data;

import java.util.List;

import br.com.skybrasil.testesky.entities.Movie;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public interface MovieService {
    @GET("Movies")
    Call<List<Movie>> getMovies();
}
