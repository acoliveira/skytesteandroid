package br.com.skybrasil.testesky;

import java.util.List;

import br.com.skybrasil.testesky.entities.Movie;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public interface MoviesContract {
    interface View {

        void setProgressIndicator(boolean active);

        void showMovies(List<Movie> movies);

        void showErro();
    }

    interface UserActionsListener {
        void loadMovies();
    }
}
