package br.com.skybrasil.testesky;

import android.support.annotation.NonNull;

import java.util.List;

import br.com.skybrasil.testesky.entities.Movie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public class MoviesPresenter implements MoviesContract.UserActionsListener {

    private MoviesContract.View view;
    private Call<List<Movie>> call;

    public MoviesPresenter(@NonNull MoviesContract.View view, @NonNull Call<List<Movie>> call) {
        this.view = checkNotNull(view);
        this.call = checkNotNull(call);
    }

    @Override
    public void loadMovies() {
        view.setProgressIndicator(true);
        call.enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                if (response.isSuccessful()) {
                    view.setProgressIndicator(false);
                    view.showMovies(response.body());
                } else {
                    new Throwable("Falha ao carregar Movies");
                }
            }

            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                view.setProgressIndicator(false);
                view.showErro();
            }
        });
    }
}
