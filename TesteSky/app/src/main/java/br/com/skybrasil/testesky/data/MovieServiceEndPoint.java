package br.com.skybrasil.testesky.data;

import java.util.List;

import br.com.skybrasil.testesky.entities.Movie;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alancostaoliveira on 12/6/17.
 */

public class MovieServiceEndPoint {
    private static final String URL_API = "https://sky-exercise.herokuapp.com/api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(URL_API)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static Call<List<Movie>> getMovies() {
        MovieService service = MovieServiceEndPoint.createService(MovieService.class);
        return service.getMovies();
    }
}
